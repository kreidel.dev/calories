# Calories - React JS Coming Soon Animated Landing Page

Link to the [Landing Page](https://calories-dev.netlify.app/)

## Features

- ReactJS
- Animated background


[![N|Calories](https://calories-dev.netlify.app/preview.png)](https://calories-dev.netlify.app/)

## Available Scripts
#### `npm start`
#### `npm run build`

